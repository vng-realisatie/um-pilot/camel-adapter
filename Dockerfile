FROM adoptopenjdk/openjdk11

ARG ARG_APP_VERSION=0.0.1-SNAPSHOT

ENV TZ=Europe/Amsterdam

EXPOSE 8080

COPY target/camel_adapter-${ARG_APP_VERSION}.jar camel-adapter.jar

ENTRYPOINT ["java","-jar","/camel-adapter.jar"]
