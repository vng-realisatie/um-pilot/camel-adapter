package nl.vng.camel_adapter.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * HttpProcessorTest
 */
class HttpProcessorTest {

    @Test
    void shouldProcess() {

        Exchange exchange = Mockito.mock(Exchange.class);
        Message message = mock(Message.class);
        when(exchange.getIn()).thenReturn(message);

        when(message.getBody(String.class)).thenReturn("");

        new HttpProcessor().process(exchange);

        Mockito.verify(exchange).getIn();
        Mockito.verify(message).getBody(String.class);
    }
}