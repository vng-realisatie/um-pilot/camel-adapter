package nl.vng.camel_adapter.processor;

import nl.vng.camel_adapter.entity.Werkzoekende;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * WerkzoekendeProcessorTest
 */
class WerkzoekendeProcessorTest {

    @Test
    void shouldProcess() {

        Exchange exchange = Mockito.mock(Exchange.class);
        Message message = mock(Message.class);
        when(exchange.getIn()).thenReturn(message);

        Werkzoekende werkzoekende = Mockito.mock(Werkzoekende.class);
        when(message.getBody()).thenReturn(werkzoekende);

        new WerkzoekendeProcessor().process(exchange);

        Mockito.verify(exchange).getIn();
        Mockito.verify(message).getBody();
    }
}