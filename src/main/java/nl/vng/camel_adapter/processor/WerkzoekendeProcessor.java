package nl.vng.camel_adapter.processor;

import nl.vng.camel_adapter.entity.Werkzoekende;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;

/**
 * WerkzoekendeProcessor
 */
@Slf4j
public class WerkzoekendeProcessor implements Processor {

    /**
     * Processes the exchange
     * @param exchange to process
     */
    @Override
    public void process(final Exchange exchange) {
        Werkzoekende werkzoekende = (Werkzoekende) exchange.getIn().getBody();
        log.info(werkzoekende.toString());
    }
}
