package nl.vng.camel_adapter.processor;

import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;

/**
 * HttpProcessor
 */
@Slf4j
public class HttpProcessor implements Processor {

    /**
     * Processes the exchange
     * @param exchange to process
     */
    @Override
    public void process(final Exchange exchange) {
        log.info(exchange.getIn().getBody(String.class));
    }
}
