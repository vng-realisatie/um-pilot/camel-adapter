package nl.vng.camel_adapter.entity;

import lombok.Getter;
import lombok.Setter;
import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;

@Getter
@Setter
@CsvRecord(separator = ",")
public class Werkzoekende {
    @DataField(pos = 1)
    private Long id;

    @DataField(pos = 2)
    private String voornaam;

    @DataField(pos = 3)
    private String achternaam;

    @DataField(pos = 4)
    private String dob;

    @DataField(pos = 5, delimiter = ";")
    private String vorigeBaan;

    @Override
    public String toString() {
        return "Werkzoekende{" +
                "id=" + id +
                ", voornaam='" + voornaam + '\'' +
                ", achternaam='" + achternaam + '\'' +
                ", dob='" + dob + '\'' +
                ", vorigeBaan=" + vorigeBaan +
                '}';
    }
}

