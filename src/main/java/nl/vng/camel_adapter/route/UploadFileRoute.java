package nl.vng.camel_adapter.route;


import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 * UploadFileRoute
 */
@Component
public class UploadFileRoute extends RouteBuilder {

    private final Environment env;

    @Autowired
    public UploadFileRoute(final Environment environment) {
        this.env = environment;
    }


    @Override
    public void configure() {
        restConfiguration()
                .component("servlet")
                .enableCORS(true)
                .corsAllowCredentials(true)
                .corsHeaderProperty("Access-Control-Allow-Origin","*")
                .corsHeaderProperty("Access-Control-Allow-Headers","Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers, Authorization");

        // Receive a mimeMultiPart file at this endpoint.
        rest("/upload/report/{oin}")
                .description("redirect json multipart via camel")
                .consumes("multipart/form-data")
                .produces("application/json")
                .post()
                .to("direct:uploadReportProcessor");
        from("direct:uploadReportProcessor")
                // Unmarshal the file.
                .unmarshal().mimeMultipart()
                // Remove CamelHttpUri header.
                .removeHeader(Exchange.HTTP_URI)
                .setHeader(Exchange.HTTP_METHOD, simple("POST"))
                .setHeader(Exchange.CONTENT_TYPE, constant("application/json"))
                // send the list of Werkzoekenden to the backend with corresponding oin as pathvariable.
                .toD(this.env.getProperty("backend.url") + "/werkzoekende/lijst/" + "${header.oin}");
    }
}
